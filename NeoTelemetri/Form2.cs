﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Media;
using NeoGalaxy.Properties;

namespace NeoGalaxy
{
    public partial class Form2 : Form
    {
        
        Thread th;
        public Form2()
        {
            InitializeComponent();
            
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics dc = e.Graphics;
            base.OnPaint(e);
        }

        private void Form2_MouseMove(object sender, MouseEventArgs e)
        {
            this.Refresh();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Refresh();
            this.Close();
            th = new Thread(opennewform);
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
        }

        private void opennewform(object obj)
        {
            Application.Run(new Form1());
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void BackSound()
        {
            SoundPlayer simplesound = new SoundPlayer(Resources.SoundBack);
            simplesound.Play();
        }
    }
}
