﻿using NeoGalaxy.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeoGalaxy
{
    class CTarget : CImageBase
    {
        private Rectangle _targethotspot = new Rectangle();

        public CTarget() : base(Resources.Target)
        {
            _targethotspot.X = Left + 20;
            _targethotspot.Y = Top - 1;
            _targethotspot.Width = 30;
            _targethotspot.Height = 40;
        }

        public void Update(int X, int Y)
        {
            Left = X;
            Top = Y;
            _targethotspot.X = Left + 20;
            _targethotspot.Y = Top - 1;
        }
        public bool Hit(int X, int Y)
        {
            Rectangle c = new Rectangle(X, Y, 1, 1); //Create a cursor
            if (_targethotspot.Contains(c))
            {
                return true;
            }
            return false;
        }
    }
}

