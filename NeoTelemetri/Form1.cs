﻿#define My_Debug //Melihat Koordinat

using NeoGalaxy.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NeoGalaxy
{
    public partial class Form1 : Form
    {
        const int FrameNum = 10;
        const int SplatNum = 10;

        bool splat = false;

        int _gameFrame = 0;
        int _splatTime = 0;

        int _hits = 0;
        int _miss = 0;
        int _totalShots = 0;
        double _averageHits = 0;

#if My_Debug
        int _cursX = 0;
        int _cursY = 0;
#endif
        CTarget _target;
        CSplat _splat;
        //CSign _sign;
        //CScoreFrame _scoreFrame;
        Random rnd = new Random();

        public Form1()
        {
            
            InitializeComponent();

            //create scope site
            Bitmap b = new Bitmap(Resources.Site);
            this.Cursor = CustomCursor.CreateCursor(b, b.Height / 2, b.Width / 2);

            _target = new CTarget() { Left = 10, Top = 200 };
            //_sign = new CSign() { Left = 340, Top = 10 };
            _splat = new CSplat();
            //_scoreFrame = new CScoreFrame() { Left = 10, Top = 10 };
            pictureBox3.BackColor = Color.Transparent;
        }

        private void timerGameLoop_Tick(object sender, EventArgs e)
        {
            if (_gameFrame >= FrameNum)
            {
                UpdateTarget();
                _gameFrame = 0;
            }

            if (splat)
            {
                if (_splatTime >= SplatNum)
                {
                    splat = false;
                    _splatTime = 0;
                    UpdateTarget();
                }
                _splatTime++;
            }
            _gameFrame++;
            this.Refresh();

            int width = this.Width; // get the width of form

            if (pictureBox1.Location.X > width - pictureBox3.Width)
            {
                pictureBox3.Location = new Point(1, pictureBox3.Location.Y); //Memulai lagi
            }
            else
            {
                pictureBox3.Location = new Point(pictureBox3.Location.X + 100, pictureBox3.Location.Y);//Menggerakkan gamar ke x
            }
        }

        private void UpdateTarget()
        {
            _target.Update(
                rnd.Next(Resources.Target.Width, this.Width - Resources.Target.Width), 
                rnd.Next(this.Height / 2, this.Height - Resources.Target.Height * 2));
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics dc = e.Graphics;

            if(splat == true)
            {
                _splat.DrawImage(dc);
            }
            else
            {
                _target.DrawImage(dc);
            }

            //_sign.DrawImage(dc);
            //_scoreFrame.DrawImage(dc);
#if My_Debug
            TextFormatFlags flags = TextFormatFlags.Left | TextFormatFlags.EndEllipsis;
            Font _font = new System.Drawing.Font("Stencil", 12, FontStyle.Regular);
            TextRenderer.DrawText(dc, "x=" + _cursX.ToString() + ":" + "Y=" + _cursY.ToString(), _font ,
                new Rectangle (0,0,120,20), SystemColors.ControlText, flags) ;
#endif
            /*//Score and Time
            TextFormatFlags flags = TextFormatFlags.Left;
            Font _font = new System.Drawing.Font("Stencil", 12, FontStyle.Regular);
            TextRenderer.DrawText(e.Graphics, "" + _totalShots.ToString(), _font, 
                new Rectangle(10,10,10,10), SystemColors.ControlText, flags);

            TextRenderer.DrawText(e.Graphics, "" + _hits.ToString(), _font,
                new Rectangle(20, 20, 20, 20), SystemColors.ControlText, flags);

            TextRenderer.DrawText(e.Graphics, "" + _miss.ToString(), _font,
                new Rectangle(30, 30, 30, 30), SystemColors.ControlText, flags);

            TextRenderer.DrawText(e.Graphics, "" + _averageHits.ToString("F0") +"%", _font,
                new Rectangle(40, 40, 40, 40), SystemColors.ControlText, flags);*/

            //_target.DrawImage(dc);

            base.OnPaint(e);
            pictureBox1.SendToBack();
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
#if My_Debug
            _cursX = e.X;
            _cursY = e.Y;
#endif
            this.Refresh();
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            
            if (e.X > 0 && e.X < 700 && e.Y > 2 && e.Y < 700) //Koordinat Menu Start
            {
                timerGameLoop.Start();
            }
            else if (e.X > 10 && e.X < 11 && e.Y > 12 && e.Y < 15)//Koordinat Menu Quit
            {
                timerGameLoop.Stop();
            }
            else
            {
                if (_target.Hit(e.X, e.Y))
                {
                    splat = true;
                    _splat.Left = _target.Left - Resources.Splat.Width / 3;
                    _splat.Top = _target.Top - Resources.Splat.Height / 3;

                    _hits++;
                }
                else
                {
                    _miss++;
                }
                _totalShots = _hits + _miss;
                _averageHits = (double)_hits / (double)_totalShots * 100.0;
            }
            firegun();
        }

        private void firegun()
        {
            SoundPlayer simplesound = new SoundPlayer(Resources.lasergun);
            simplesound.Play();
        }

        private void BackSound()
        {
            SoundPlayer simplesound = new SoundPlayer(Resources.SoundBack);
            simplesound.Play();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //BackSound();
            timerGameLoop.Start();
        }
    }
}